FROM osrf/ros:melodic-desktop-full-bionic

ARG uid=1000
ARG gid=1000

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENV USERNAME ros
RUN useradd -m $USERNAME && \
        echo "$USERNAME:$USERNAME" | chpasswd && \
        usermod --shell /bin/bash $USERNAME && \
        usermod -aG sudo $USERNAME && \
        echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/$USERNAME && \
        chmod 0440 /etc/sudoers.d/$USERNAME && \
        # Replace 1000 with your user/group id
        usermod  --uid $uid $USERNAME && \
        groupmod --gid $gid $USERNAME

RUN apt-get update && apt-get install -y \
      python-catkin-tools bash \
      mesa-utils libgl1-mesa-glx 
WORKDIR /home/$USERNAME
COPY src /home/$USERNAME/src
RUN rosdep install --from-paths . -r -y
RUN catkin config \
      --extend /opt/ros/melodic && \
    catkin build

ENTRYPOINT ["/entrypoint.sh"]
